# Copyright 2019 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ tag=clang_${PV} ] cmake [ api=2 ]

SUMMARY="Find unnecessary include directives in C/C++ programs"
HOMEPAGE+=" https://include-what-you-use.org/"

LICENCES="GPL-2"
SLOT="7"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-lang/llvm:${SLOT}
        dev-lang/clang:${SLOT}
"

LLVM_PREFIX="/usr/$(exhost --target)/lib/llvm/${SLOT}"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_INSTALL_PREFIX:PATH="${LLVM_PREFIX}"
)

src_install() {
    cmake_src_install

    # Symlinked binaries to /usr/$(exhost --target)/bin (without .py extension and with -${SLOT}
    # suffix, e.g. `iwyu-tool-${SLOT}`)
    dodir "/usr/$(exhost --target)/bin"
    edo pushd "${IMAGE}${LLVM_PREFIX}/bin"
    for bin in *; do
        dosym "${LLVM_PREFIX}/bin/${bin}" "/usr/$(exhost --target)/bin/${bin%*.py}-${SLOT}"
    done
    edo popd
}

